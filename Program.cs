﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using Telegram.Bot;
using RawPrint;
using System.Net;
using Newtonsoft.Json;
using System.IO;

namespace BotQwe
{
    class Program
    {
        public static TelegramBotClient Client;
        
        static void Main(string[] args)
        {
            Client = new TelegramBotClient("1449653211:AAEw0OckGnUwdNEu9CHsaqrozeedeyCMrCQ");
            Client.OnMessage += Client_OnMessage;
            Client.StartReceiving();
            Thread.Sleep(-1);
        }

        private static void Client_OnMessage(object sender, Telegram.Bot.Args.MessageEventArgs e)
        {
            var id = e.Message.Chat.Id;
            var text = e.Message.Text;
            var response = "";

            text = text.Substring(1);
            text = text.Split(' ')[0];
            //Client.SendTextMessageAsync(id, $"Youd said: {text}");
            Client.SendTextMessageAsync(id, text);

            //switch (text)
            //{
            //    case "now":
            //        response = DateTime.Now.ToString();

            //        //Client.SendTextMessageAsync(id, response);
            //        break;
            //    case "id":
            //        response = id.ToString();

            //        //Client.SendTextMessageAsync(id, response);
            //        break;

            //    case "say":
            //        response = "Hello";

            //        //Client.SendTextMessageAsync(id, response);
            //        break;

            //    case "p":
            //        response = "Iya Sayang <3 <3 <3";

            //        //Client.SendTextMessageAsync(id, response);
            //        break;

            //    case "adin":
            //        response = "M A H O";
            //        break;

            //    case "winword":
            //        //string wind = "C:\Program Files(x86)\TeamViewer\TeamViewer.exe";
            //        //string winpath = Environment.GetEnvironmentVariable("windir");

            //        Process.Start(@"C:\Program Files(x86)\TeamViewer\TeamViewer.exe");
            //        response = "Run";
            //        break;

            //    case "print":

            //        response = GetPrint();
            //        break;

            //    default:
            //        response = "Jok Ngadi Ngadi !";
            //        break;
            //}

            //Client.SendTextMessageAsync(id, response);
        }

        private const int NumberOfRetries = 3;
        private const int DelayOnRetry = 1000;
        static string GetPrint()
        {
            string strResponse = string.Empty;
            string Url = string.Empty;
            string Data = string.Empty;

            RestClient rClient = new RestClient();
            WebClient client = new WebClient();

            try {
                rClient.endPoint = "http://wijayaplywoodsindonesia.com/api/print";
                rClient.httpMethod = httpVerb.GET;
                strResponse = rClient.makeRequest();
                var JSONv = JsonConvert.DeserializeObject<PrintTask>(strResponse);
                if (JSONv.Id != null) {
                    Url = JSONv.Url;
                    Data = JSONv.Url;

                    Uri uri = new Uri(Url);
                    string filenameunduh = Path.GetFileName(uri.AbsolutePath);
                    client.DownloadFileAsync(uri, "file/"+filenameunduh);

                    string Filepath = Path.Combine(Environment.CurrentDirectory, @"file\" + filenameunduh);
                    //string Printername = "EPSON L120 Series";
                    string Printername = "Microsoft Print to PDF";

                    IPrinter printer = new Printer();
                    //try {
                    //    IPrinter printer = new Printer();
                    //    printer.PrintRawFile(Printername, Filepath, filenameunduh);
                    //    printer.Prin
                    //}
                    //catch (Exception exc) {
                    //    Data = exc.Message;
                    //}
                    for (int i = 1; i <= NumberOfRetries; ++i)
                    {
                        try
                        {
                            // Do stuff with file
                            printer.PrintRawFile(Printername, Filepath, filenameunduh);
                            break; // When done we can break loop
                        }
                        catch (IOException e) when (i <= NumberOfRetries)
                        {
                            // You may check error code to filter some exceptions, not every error
                            // can be recovered.
                            Thread.Sleep(DelayOnRetry);
                        }
                    }
                }
                Data = "Tercetak";
                //Client.SendTextMessageAsync(id, "print");
            } catch (Exception ex) {
                Data = ex.Message;
                //Console.WriteLine(ex.Message);
            }

            return Data;
        }
    }
}
